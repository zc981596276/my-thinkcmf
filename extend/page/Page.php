<?php
namespace page;
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2017 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: zhangyajun <448901948@qq.com>
// +----------------------------------------------------------------------
use think\Paginator;
class Page extends Paginator
{
    //首页
    protected function home() {
        if ($this->currentPage() > 1) {
            return "<a href='" . $this->url(1) . "' class='page-btn' title='首页'>首页</a>";
        } else {
            return "<a class='page-btn not-allowed' title='首页'>首页</a>";
        }
    }
    //上一页
    protected function prev() {
        if ($this->currentPage() > 1) {
            return "<a href='" . $this->url($this->currentPage - 1) . "'class='page-btn' title='上一页'>上一页</a>";
        } else {
            return "<a class='page-btn not-allowed' title='上一页'>上一页</a>";
        }
    }
    //下一页
    protected function next() {
        if ($this->hasMore) {
            return "<a href='" . $this->url($this->currentPage + 1) . "'class='page-btn' title='下一页'>下一页</a>";
        } else {
            return "<a class='page-btn not-allowed' title='下一页'>下一页</a>";
        }
    }
    //尾页
    protected function last() {
        if ($this->hasMore) {
            return "<a href='" . $this->url($this->currentPage + 1) . "'class='page-btn' title='尾页'>尾页</a>";
        } else {
            return "<a class='page-btn not-allowed' title='尾页'>尾页</a>";
        }
    }
    //统计信息
    protected function info(){
//        return "<p class='pageRemark'>共<b>" . $this->lastPage .
//        "</b>页<b>" . $this->total . "</b>条数据</p>";
        return "  共 {$this->lastPage} 页 {$this->total} 条记录";
    }
    /**
     * 页码按钮
     * @return string
     */
    protected function getLinks()
    {
        $block = [
            'first'  => null,
            'slider' => null,
            'last'   => null
        ];
        $side   = 3;
        $window = $side * 2;
        if ($this->lastPage < $window + 6) {
            $block['first'] = $this->getUrlRange(1, $this->lastPage);
        } elseif ($this->currentPage <= $window) {
            $block['first'] = $this->getUrlRange(1, $window + 2);
            $block['last']  = $this->getUrlRange($this->lastPage - 1, $this->lastPage);
        } elseif ($this->currentPage > ($this->lastPage - $window)) {
            $block['first'] = $this->getUrlRange(1, 2);
            $block['last']  = $this->getUrlRange($this->lastPage - ($window + 2), $this->lastPage);
        } else {
            $block['first']  = $this->getUrlRange(1, 2);
            $block['slider'] = $this->getUrlRange($this->currentPage - $side, $this->currentPage + $side);
            $block['last']   = $this->getUrlRange($this->lastPage - 1, $this->lastPage);
        }
        $html = '';
        if (is_array($block['first'])) {
            $html .= $this->getUrlLinks($block['first']);
        }
        if (is_array($block['slider'])) {
            $html .= $this->getDots();
            $html .= $this->getUrlLinks($block['slider']);
        }
        if (is_array($block['last'])) {
            $html .= $this->getDots();
            $html .= $this->getUrlLinks($block['last']);
        }
        return $html;
    }
    /**
     * 渲染分页html
     * @return mixed
     */
    public function render()
    {
        if ($this->hasPages()) {
            if ($this->simple) {
                return sprintf(
                    '%s  <div class=\'page-contain\'>
        <div id="pages">%s %s %s</div>
    </div>',
                    $this->css(),
                    $this->prev(),
                    $this->getLinks(),
                    $this->next()
                );
            } else {
                return sprintf(
                    '%s<div class="pagination">%s %s %s %s %s %s</div>',
                    $this->css(),
                    $this->home(),
                    $this->prev(),
                    $this->getLinks(),
                    $this->next(),
                    $this->last(),
                    $this->info()
                );
            }
        }
    }
    /**
     * 生成一个可点击的按钮
     *
     * @param  string $url
     * @param  int    $page
     * @return string
     */
    protected function getAvailablePageWrapper($url, $page)
    {
        return '<a href="' . htmlentities($url) . '" class="page-btn" title="第"'. $page .'"页" >' . $page . '</a>';
    }
    /**
     * 生成一个禁用的按钮
     *
     * @param  string $text
     * @return string
     */
    protected function getDisabledTextWrapper($text)
    {
        return '<p class="pageEllipsis">' . $text . '</p>';
    }
    /**
     * 生成一个激活的按钮
     *
     * @param  string $text
     * @return string
     */
    protected function getActivePageWrapper($text)
    {
        return '<a href="" class="cur">' . $text . '</a>';
    }
    /**
     * 生成省略号按钮
     *
     * @return string
     */
    protected function getDots()
    {
        return $this->getDisabledTextWrapper('...');
    }
    /**
     * 批量生成页码按钮.
     *
     * @param  array $urls
     * @return string
     */
    protected function getUrlLinks(array $urls)
    {
        $html = '';
        foreach ($urls as $page => $url) {
            $html .= $this->getPageLinkWrapper($url, $page);
        }
        return $html;
    }
    /**
     * 生成普通页码按钮
     *
     * @param  string $url
     * @param  int    $page
     * @return string
     */
    protected function getPageLinkWrapper($url, $page)
    {
        if ($page == $this->currentPage()) {
            return $this->getActivePageWrapper($page);
        }
        return $this->getAvailablePageWrapper($url, $page);
    }
    /**
     * 分页样式
     */
    protected function css(){
        return '  <style type="text/css">
         .page-contain{
           width: 880px;display: block;text-align:center
         }
        .page-btn{
            padding: 8px 16px;
            margin: 0px 5px;
            display: inline-block;
            color: #008CBA;
            border: 1px solid #F2F2F2;
            border-radius: 5px;
            text-decoration:none;
            font-size:12px;
        }
        .not-allowed{
            cursor: not-allowed;
        }
        .page-btn:hover{
            text-decoration: none;
            background: #F8F5F5;
        }
        .current,.page-goto{
            padding: 8px 16px;
            margin: 0px 5px;
            display: inline-block;
            background-color: #008CBA;
            color: #FFF;
            font-size:12px;
            border-radius: 5px;
            border: 1px solid #008CBA;
        }
        .page-goto{
            margin:0 10px;
            font-size: 14px;
        }
        .page-input{
            box-sizing: border-box;
            border: solid 1px #ddd;
            width: 90px;
            border-radius: 4px;
            padding: 8px 10px;
            height: 34px;
            font-size: 14px;
            margin-right: 10px;
            -webkit-transition: all .2s linear 0s;
            -moz-transition: all .2s linear 0s;
            -o-transition: all .2s linear 0s;
            transition: all .2s linear 0s;
        }
        .page-input:hover {
            border: solid 1px #3bb4f2;
        }
        </style>';
    }
}