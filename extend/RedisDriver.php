<?php

/**
 *
 * 统一redis的配置与数据存储规范，便于扩展与修改
 * # redis通常用于热数据与消息列队等场景
 * # list内存储array是采用json格式
 *
 */
//
use think\facade\Config;
class RedisDriver
{

    protected $redis; // redis对象
    protected $ip = '127.0.0.1'; // redis服务器ip地址
    protected $port = '16379'; // redis服务器端口
    protected $passwd = '123456'; // redis密码

    public function __construct($config = array(),$db=null)
    {
        $this->redis = new  \Redis();
        $this->connect($config,$db);
    }

    // 连接redis服务器
    public function connect($config = array(),$db=null)
    {
        $redis_config =  Config::get("config.redis_config");
        $this->ip = !empty($redis_config['host']) ? $redis_config['host'] : $this->ip;
        $this->port = !empty($redis_config['port']) ? $redis_config['port'] : $this->port;
        $this->passwd = !empty($redis_config['passwd']) ? $redis_config['passwd'] : $this->passwd;
        if (!empty($config)) {
            $this->ip = $config['ip'];
            $this->port = $config['port'];
            if (isset($config['passwd'])) {
                $this->passwd =  $config['passwd'];
            }
        }


        $state = $this->redis->connect($this->ip, $this->port);
        if ($state == false) {
            die('redis connect failure');
        }
        if (!is_null($this->passwd)) {
            $this->redis->auth($this->passwd);
        }

        if(!is_null($db)){
            $this->redis->select($db);
        }
    }

    // 设置一条String
    public function setStr($key, $text, $expire = null)
    {
        $key = 'string:' . $key;
        $this->redis->set($key, $text);
        if (!is_null($expire)) {
            $this->redis->setTimeout($key, $expire);
        }
    }

    // 获取一条String
    public function getStr($key,$field=null)
    {
        if(!$field){
            $key = 'string:' . $key;
        }

        $text = $this->redis->get($key);
        return empty($text) ? null : $text;
    }

    // 删除一条String
    public function delStr($key)
    {
        $key = 'string:' . $key;
        $this->redis->del($key);
    }

    // 设置一条Hash
    public function setHash($key, $arr, $expire = null)
    {
        $key = 'hash:' . $key;
        $this->redis->hMset($key, $arr);
        if (!is_null($expire)) {
            $this->redis->setTimeout($key, $expire);
        }
    }

    // 获取一条Hash，$fields可为字符串或数组
    public function getHash($key, $fields = null)
    {
        $key = 'hash:' . $key;
        if (is_null($fields)) {
            $arr = $this->redis->hGetAll($key);
        } else {
            if (is_array($fields)) {
                $arr = $this->redis->hmGet($key, $fields);
                foreach ($arr as $key => $value) {
                    if ($value === false) {
                        unset($arr[$key]);
                    }
                }
            } else {
                $arr = $this->redis->hGet($key, $fields);
            }
        }
        return empty($arr) ? null : (is_array($arr) ? $arr : array($fields => $arr));
    }

    // 删除一条Hash，$field为字符串
    public function delHash($key, $field = null)
    {
        $key = 'hash:' . $key;
        if (is_null($field)) {
            $this->redis->del($key);
        } else {
            $this->redis->hDel($key, $field);
        }
    }

    //获取hash 所有字段值
    public function hKeys($key){
        $key = 'hash:' . $key;
        return $this->redis->hKeys($key);
    }

    //获取hash 所有值
    public function  hVals($key){
        $key = 'hash:' . $key;
        return $this->redis->hVals($key);
    }

    // 在Hash的field内增加一个值 (值之间使用“,”分隔)
    public function fieldAddVal($key, $field, $val)
    {
        $arr = $this->getHash($key, $field);
        if (!is_null($arr)) {
            $str = reset($arr);
            $arr = explode(',', $str);
            foreach ($arr as $v) {
                if ($v == $val) {
                    return;
                }
            }
            $str .= ",{$val}";
            $this->setHash($key, array($field => $str));
        } else {
            $this->setHash($key, array($field => $val));
        }
    }

    // 在Hash的field内删除一个值
    public function fieldDelVal($key, $field, $val)
    {
        $arr = $this->getHash($key, $field);
        if (!is_null($arr)) {
            $arr = explode(',', reset($arr));
            $tmpStr = '';
            foreach ($arr as $v) {
                if ($v != $val) {
                    $tmpStr .= ",{$v}";
                }
            }
            if ($tmpStr == '') {
                $this->delHash($key, $field);
            } else {
                $this->setHash($key, array($field => substr($tmpStr, 1)));
            }
        }
    }

    // 设置表格的一行数据
    public function setTableRow($table, $id, $arr, $expire = null)
    {
        $key = '' . $table . ':' . $id;
        $this->redis->hMset($key, $arr);
        if (!is_null($expire)) {
            $this->redis->setTimeout($key, $expire);
        }
    }

    // 获取表格的一行数据，$fields可为字符串或数组
    public function getTableRow($table, $id, $fields = null)
    {
        $key = '' . $table . ':' . $id;
        if (is_null($fields)) {
            $arr = $this->redis->hGetAll($key);
        } else {
            if (is_array($fields)) {
                $arr = $this->redis->hmGet($key, $fields);
                foreach ($arr as $key => $value) {
                    if ($value === false) {
                        unset($arr[$key]);
                    }
                }
            } else {
                $arr = $this->redis->hGet($key, $fields);
            }
        }
        return empty($arr) ? null : (is_array($arr) ? $arr : array($fields => $arr));
    }

    // 删除表格的一行数据
    public function delTableRow($table, $id)
    {
        $key = '' . $table . ':' . $id;
        $this->redis->del($key);
    }

    // 推送一条数据至列表，头部
    public function pushList($key, $arr)
    {
        $key = 'list:' . $key;
        $this->redis->lPush($key, json_encode($arr));
    }

    // 从列表拉取一条数据，尾部
    public function pullList($key, $timeout = 0)
    {
        $key = 'list:' . $key;
        if ($timeout > 0) {
            $val = $this->redis->brPop($key, $timeout); // 该函数返回的是一个数组, 0=key 1=value
        } else {
            $val = $this->redis->rPop($key);
        }
        $val = is_array($val) && isset($val[1]) ? $val[1] : $val;
        return empty($val) ? null : $this->objectToArray(json_decode($val));
    }

    // 取得列表的数据总条数
    public function getListSize($key)
    {
        $key = 'list:' . $key;
        return $this->redis->lSize($key);
    }

    // 删除列表
    public function delList($key)
    {
        $key = 'list:' . $key;
        $this->redis->del($key);
    }

    // 使用递归，将stdClass转为array
    protected function objectToArray($obj)
    {
        if (is_object($obj)) {
            $arr = (array) $obj;
        }
        if (is_array($obj)) {
            foreach ($obj as $key => $value) {
                $arr[$key] = $this->objectToArray($value);
            }
        }
        return !isset($arr) ? $obj : $arr;
    }
    
    
    //hash  自增
    public function hIncrBy($key, $hashKey, $value ){
        $key = "hash:".$key;
        return $this->redis->hIncrBy( $key, $hashKey, $value );
    }

    //获取 hash 某字段值
    public function hGet($key,$hashKey,$isRealKey=false){
        if(!$isRealKey){
            $key = "hash:".$key;
        }
        return $this->redis->hGet($key, $hashKey);
    }
    //批量获取hash值
    public function hMget($key,$fields = [],$isRealKey = false){
        if(!$isRealKey){
            $key = "hash:".$key;
        }
        $arr = $this->redis->hmGet($key, $fields);
        foreach($arr as $k=>&$v){
            if($v === false){
                unset($arr[$k]);
            }
        }
        return $arr;
    }
    //设置 hash 某字段值
    public function hSet($key,$hashKey,$value){
        $key = "hash:".$key;
        return $this->redis->hSet($key, $hashKey,$value);
    }

    //删除hash 某字段值
    public function hDel( $key, $hashKey ){
        $key = "hash:".$key;
        return $this->redis->hDel($key, $hashKey);
    }

    public function publish($key,$message){
        return $this->redis->publish($key, $message);
    }

    public function subscribe($channels,$callback){
        return $this->redis->subscribe($channels, $callback);
    }
}
