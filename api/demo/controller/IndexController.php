<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: pl125 <xskjs888@163.com>
// +----------------------------------------------------------------------

namespace api\demo\controller;

use cmf\controller\RestBaseController;
use GuzzleHttp\Psr7\Response;
use QL\QueryList;
use GuzzleHttp\Exception\RequestException;

class IndexController extends RestBaseController
{
    public function index()
    {
        $this->success('请求成功!', ['test' => 'test']);
    }

    //贴吧
    public function tieba()
    {
        $headerStr ='Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3
Accept-Encoding: gzip, deflate, br
Accept-Language: zh-CN,zh;q=0.9
Cache-Control: max-age=0
Connection: keep-alive
Cookie: SUV=006DE21E777B98B65D6DE2ADE4DF7573; CXID=EC3822DA411C3501CE9F3F022C8134E9; SUID=F1997B773865860A5D9EA7AC000C4CF0; ABTEST=6|1571888585|v1; IPLOC=CN4403; weixinIndexVisited=1; ad=A@l$SZllll2NX$W7lllllVL8Z51lllllNTmBDyllll9lllllpAoll5@@@@@@@@@@
DNT: 1
Host: weixin.sogou.com
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: none
Sec-Fetch-User: ?1
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36';
        $headerArr = explode(PHP_EOL, $headerStr);
        $headers = [];
        foreach ($headerArr as $k => $v) {
            $row = explode(":", $v);
            $headers[$row[0]] = $row[1];
        }

        $urls = [
            'https://weixin.sogou.com/?p=73141200',
        ];

        $rules = [
            'name' => ['ul>li>div>h3>a', 'text'],
            'desc' => ['ul>li>div>p', 'text'],
            'img' => ['ul>li>div>a>img', 'src'],
        ];
        $range = '';
        QueryList::rules($rules)
            ->range($range)
            ->multiGet($urls)
            // 设置并发数为2
            ->concurrency(2)
            // 设置GuzzleHttp的一些其他选项
            ->withOptions([
                'timeout' => 60,
                'verify' => false,
            ])
            // 设置HTTP Header
            ->withHeaders($headers)
            // HTTP success回调函数
            ->success(function (QueryList $ql, Response $response, $index) {
                $data = $ql->queryData();
                print_r($data);
            })
            // HTTP error回调函数
            ->error(function (QueryList $ql, $reason, $index) {
                $reason = $reason->getHandlerContext();
                var_dump($reason['error']);
            })
            ->send();
    }


}
