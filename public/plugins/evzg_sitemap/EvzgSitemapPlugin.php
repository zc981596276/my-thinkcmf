<?php
//
// XML网站地图生成插件
// 
// 
// Copyright 2018, 壹味科技
// http://www.evzg.com/
// 
// Author:TianQi,GaoYong
// Released on:2018-05-24
//
namespace plugins\evzg_sitemap;
use cmf\lib\Plugin;

class EvzgSitemapPlugin extends Plugin
{

    public $info = [
        'name'        => 'EvzgSitemap',
        'title'       => '生成XML地图',
        'description' => '生成XML地图',
        'status'      => 1,
        'author'      => '壹味科技',
        'version'     => '1.0',
        'demo_url'    => '',
        'author_url'  => 'https://www.evzg.com'
    ];

    public $hasAdmin = 1;

    public function install()
    {
        return true;
    }

    public function uninstall()
    {
        $file = $_SERVER['DOCUMENT_ROOT']."/sitemap."."xml";
        if (!unlink($file))
          {
         return false;
          }
        else
          {
          return true;
          }
    }

}