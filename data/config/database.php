<?php
/**
 * 配置文件
 */

return [
    // 数据库类型
    'type'     => 'mysql',
    // 服务器地址
    'hostname' => '127.0.0.1',
    // 数据库名
    'database' => 'thinkcmf',
    // 用户名
    'username' => 'root',
    // 密码
    'password' => 'admin',
    // 端口
    'hostport' => '3306',
    // 数据库编码默认采用utf8
    'charset'  => 'utf8mb4',
    // 数据库表前缀
    'prefix'   => 'cmf_',
    "authcode" => 'uCYcnR0X55Ezk5sK8x',
    //#COOKIE_PREFIX#
];
