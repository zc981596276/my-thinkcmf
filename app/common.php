<?php
// 公共函数

//分割中文
function sliceArticle($article,$limit=4900){
    $res = [];
    $len = ceil(mb_strlen($article)/$limit);
    for($i=0;$i<$len;$i++){
        if($i == $len){
            $res[] = substr_text($article,$i*$limit,($i*$limit) -mb_strlen($article),"utf-8");
        }else{
            $res[] =  substr_text($article,$i*$limit,$limit,"utf-8");
        }
    }

    return $res;
}


function substr_text($str, $start=0, $length, $charset="utf-8", $suffix="")
{
    if(function_exists("mb_substr")){
        return mb_substr($str, $start, $length, $charset).$suffix;
    }
    elseif(function_exists('iconv_substr')){
        return iconv_substr($str,$start,$length,$charset).$suffix;
    }
    $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
    $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
    $re['gbk']  = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
    $re['big5']  = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
    preg_match_all($re[$charset], $str, $match);
    $slice = join("",array_slice($match[0], $start, $length));
    return $slice.$suffix;
}