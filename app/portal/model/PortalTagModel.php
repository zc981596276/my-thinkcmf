<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author:kane < chengjin005@163.com>
// +----------------------------------------------------------------------
namespace app\portal\model;

use think\Model;
use think\db\Where;

class PortalTagModel extends Model
{
    public static $STATUS = array(
        0 => "未启用",
        1 => "已启用",
    );

    /* author@zhou
     * 功能：获取发布的标签 按最大数
     * return
     */
    public function getTagLists($page, $pageSize = 12)
    {
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $host = $http_type . $_SERVER['SERVER_NAME'] . '/';
        $routeModel = new \app\portal\model\RouteModel();
        $row = $routeModel->where(array("status" => 1, "full_url" => "portal/Tag/index"))->find();
        $data = $this->where(array("status" => 1))->limit(($page - 1) * $pageSize, $pageSize)->order("recommended desc")->select();
        foreach ($data as $k => &$v) {
            $real_url = str_replace(":id", $v['id'], $row['url']);
            $real_url = $host . $real_url . ".html";
            $v['real_url'] = $real_url;
        }

        return $data;
    }


    /* author@zhou
     * 功能：获取热门标签
     * return 
     */
    public function hotTagLists($page, $pageSize = 12)
    {
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $host = $http_type . $_SERVER['SERVER_NAME'] . '/';
        $routeModel = new \app\portal\model\RouteModel();
        $row = $routeModel->where(array("status" => 1, "full_url" => "portal/Tag/index"))->find();
        $data = $this->where(array("status" => 1))->limit(($page - 1) * $pageSize, $pageSize)->order("post_count desc")->select();
        foreach ($data as $k => &$v) {
            $real_url = str_replace(":id", $v['id'], $row['url']);
            $real_url = $host . $real_url . ".html";
            $v['real_url'] = $real_url;
        }

        return $data;
    }


    /* author@zhou
     * 功能：相关推荐
     * return 
     */
    public function relatedTagLists($words, $page, $pageSize = 12)
    {
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $host = $http_type . $_SERVER['SERVER_NAME'] . '/';
        $routeModel = new \app\portal\model\RouteModel();
        $row = $routeModel->where(array("status" => 1, "full_url" => "portal/Tag/index"))->find();
        $model = $this;
        foreach ($words as $k => $v) {
            $model = $model->whereOr("name", "like", "%" . $v);
        }


        $data = $model->limit(($page - 1) * $pageSize, $pageSize)->order("id desc")->select();
        foreach ($data as $k => &$v) {
            $real_url = str_replace(":id", $v['id'], $row['url']);
            $real_url = $host . $real_url . ".html";
            $v['real_url'] = $real_url;
        }

        return $data;
    }


    /* author@zhou
     * 功能：通过文章id 查找标签
     * return 
     */
    public function articleTagLists($articleIds = [])
    {

        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $host = $http_type . $_SERVER['SERVER_NAME'] . '/';
        $opt['p.status'] = 1;
        $opt['p.post_id'] = array("IN", $articleIds);
        $data = $this->field("a.*,p.post_id")->table("cmf_portal_tag a")->where(new Where($opt))->join("cmf_portal_tag_post p", 'p.tag_id = a.id', 'left')->select();

        $routeModel = new \app\portal\model\RouteModel();
        $row = $routeModel->where(array("status" => 1, "full_url" => "portal/Tag/index"))->find();
        foreach ($data as $k => &$v) {
            $real_url = str_replace(":id", $v['id'], $row['url']);
            $real_url = $host . $real_url . ".html";
            $v['real_url'] = $real_url;
        }
        $result = [];
        foreach ($data as $k => $v) {
            $result[$v['post_id']][] = $v;
        }

        return $result;
    }


    /* author@zhou
     * 功能 通过标签id查找文章
     * return
     */
    public function tagArticleLists($tag_id = '')
    {
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $host = $http_type . $_SERVER['SERVER_NAME'] . '/';
        $opt['p.status'] = array("eq",1);
        $opt['p.tag_id'] = array("eq", $tag_id);
        $ids = $this->field("a.*,p.post_id")->table("cmf_portal_tag a")->where(new Where($opt))->join("cmf_portal_tag_post p", 'p.tag_id = a.id')->column('post_id');

        $postModel = new \app\portal\model\PortalPostModel();
        $opt1['post_status'] = array("eq",1);
        $opt1['cmf_portal_post.id'] = array("IN",$ids);
        $data = $postModel->field("cmf_portal_post.*,c.category_id")->where(new Where($opt1))->join("cmf_portal_category_post c","c.post_id = cmf_portal_post.id",'left')->paginate();

        return $data;
    }
}