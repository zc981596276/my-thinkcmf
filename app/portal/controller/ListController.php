<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 老猫 <thinkcmf@126.com>
// +----------------------------------------------------------------------
namespace app\portal\controller;

use cmf\controller\HomeBaseController;
use app\portal\model\PortalCategoryModel;
use app\portal\model\PortalPostModel;
use app\portal\model\PortalTagModel;


class ListController extends HomeBaseController
{
    /***
     * 文章列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        //通过id查找子类
        $id                  = $this->request->param('id', 0, 'intval');
        $page                 = $this->request->param('page', 1, 'intval');
        $pageSize                 =15;

        $portalCategoryModel = new PortalCategoryModel();
        $portalPostModel = new PortalPostModel();
        $portalTagModel = new PortalTagModel();
        $category = $portalCategoryModel->where('id', $id)->where('status', 1)->find();

        //三级架构
        $cateIds = [];
        $paths = explode('-',$category['path']);
        if(count($paths) == 3){
            $firstdata = $portalCategoryModel->getChildrenCate(0);
            $seconddata = $portalCategoryModel->getChildrenCate($paths[1]);

        }else{
            $firstdata = $portalCategoryModel->getChildrenCate(0);
            $seconddata = $portalCategoryModel->getChildrenCate($id);
        }
        $cateIds=$paths;
        //面包谐导航
        $miaobao = $portalCategoryModel->mianbao($paths);
        //对应类型的数据
        $data = $portalPostModel->getCatelistData($id,$page,$pageSize);
        //相关标签
        $s = json_decode(json_encode($data),true);
        $tagLists =$portalTagModel->articleTagLists(array_column($s,'id'));
        foreach($data as $k=>$v){
            $v['taglist'] = isset($tagLists[$v['id']]) ? $tagLists[$v['id']] : [];
        }



        $list = $portalPostModel->pagnate($id,$page,$pageSize);//分页
        $pages = $list->render();
        //阅读排行榜
        $routeData = $portalPostModel->getRouteData();
        $hotData = $portalPostModel->hotArticleLists($page,8,$routeData,"*",$id);
        //热门标签
        $hotTagData = $portalTagModel->hotTagLists($page,$pageSize = 12);

        //相关标签
        $word = [];
        foreach($miaobao as $k=>$v){
            if($k > 0){
                $word[] =$this->autoWord($v['name']);
            }
        }
        $words =  array_reduce($word, 'array_merge', array());
        $relateTagData = $portalTagModel->relatedTagLists($words,$page, 12);



        //分页
        $this->assign('category', $category);
        $this->assign("firstdata",$firstdata);
        $this->assign("seconddata",$seconddata);
        $this->assign("miaobao",$miaobao);
        $this->assign("data",$data);
        $this->assign("hotData",$hotData);
        $this->assign("hotTagData",$hotTagData);
        $this->assign("relateTagData",$relateTagData);
        $this->assign("cateIds",$cateIds);//分类id判断active
        $this->assign("pages",$pages);//分页

        $listTpl = empty($category['list_tpl']) ? 'list' : $category['list_tpl'];
        return $this->fetch('/' . $listTpl);
    }








}
