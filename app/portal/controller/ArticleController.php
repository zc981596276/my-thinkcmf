<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 老猫 <thinkcmf@126.com>
// +----------------------------------------------------------------------
namespace app\portal\controller;

use cmf\controller\HomeBaseController;
use app\portal\model\PortalCategoryModel;
use app\portal\service\PostService;
use app\portal\model\PortalPostModel;
use app\portal\model\PortalTagModel;
use think\Db;



class ArticleController extends HomeBaseController
{
    /**
     * 文章详情
     * @return mixed
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {

        $portalCategoryModel = new PortalCategoryModel();
        $postService         = new PostService();
        $PortalTagModel         = new PortalTagModel();

        $articleId  = $this->request->param('id', 0, 'intval');
        $categoryId = $this->request->param('cid', 0, 'intval');


        $article    = $postService->publishedArticle($articleId, $categoryId);


        if (empty($article)) {
            abort(404, '文章不存在!');
        }

//        $prevArticle = $postService->publishedPrevArticle($articleId, $categoryId);
//        $nextArticle = $postService->publishedNextArticle($articleId, $categoryId);


        $tplName = 'article';

        if (empty($categoryId)) {
            $categories = $article['categories'];

            if (count($categories) > 0) {
                $this->assign('category', $categories[0]);
            } else {
                abort(404, '文章未指定分类!');
            }

        } else {
            $category = $portalCategoryModel->where('id', $categoryId)->where('status', 1)->find();

            if (empty($category)) {
                abort(404, '文章不存在!');
            }

            $this->assign('category', $category);

            $tplName = empty($category["one_tpl"]) ? $tplName : $category["one_tpl"];
        }

        Db::name('portal_post')->where('id', $articleId)->setInc('post_hits');


        //面包屑导航
        $PortalPostModel = new PortalPostModel();
        $PortalCategoryModel = new PortalCategoryModel();

        $miaobao = $PortalCategoryModel->mianbao(explode('-',$article['path']));

        //相关文章
        $word = [];
        foreach($miaobao as $k=>$v){

            if($k > 0){
                $word[] =$this->autoWord($v['name']);
            }
        }
        $word[] = $this->autoWord($article['post_title']);
        $words =  array_reduce($word, 'array_merge', array());
        $relateData = $PortalPostModel->relatedLists($words,1,8);
        //相关标签
        $relateTagData = $PortalTagModel->relatedTagLists($words,1,20);

        //精选文章
        $routeData = $PortalPostModel->getRouteData();
        $recommData = $PortalPostModel->recommArticleLists(1,12,$routeData,"*",$categoryId);
        //var_dump($recommData);die;

        hook('portal_before_assign_article', $article);
        $this->assign('article', $article);
        //$this->assign('prev_article', $prevArticle);
        //$this->assign('next_article', $nextArticle);
        $this->assign('relateData', $relateData);
        $this->assign('relateTagData', $relateTagData);
        $this->assign('miaobao', $miaobao);
        $this->assign('recommData', $recommData);

        $tplName = empty($article['more']['template']) ? $tplName : $article['more']['template'];
        return $this->fetch("/$tplName");
    }

    // 文章点赞
    public function doLike()
    {
        //$this->checkUserLogin();
        $articleId = $this->request->param('id', 0, 'intval');


       // $canLike = cmf_check_user_action("posts$articleId", 1);


        Db::name('portal_post')->where('id', $articleId)->setInc('post_like');
        echo json_encode(["error_code"=>200,"msg"=>"赞好啦"]);
        return;

    }

}
