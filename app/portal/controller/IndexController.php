<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 老猫 <thinkcmf@126.com>
// +----------------------------------------------------------------------
namespace app\portal\controller;

use cmf\controller\HomeBaseController;
use app\portal\model\PortalPostModel;
use app\portal\model\PortalTagModel;
use Stichoza\GoogleTranslate\GoogleTranslate;


class IndexController extends HomeBaseController
{
    public function index()
    {
        //分页类
        $pageSize = 10;
        $list = PortalPostModel::where('post_status', 1)->paginate($pageSize);
        $pages = $list->render();


        //最新文章 12篇
        $page = $this->request->param('page', 1);
        $postModel = new PortalPostModel();
        $tagModel = new PortalTagModel();
        $routeData = $postModel->getRouteData();
        $newData = $postModel->NewArticleLists($page, $pageSize, $routeData);
        //相关标签
        $s = json_decode(json_encode($newData), true);
        $tagLists = $tagModel->articleTagLists(array_column($s, 'id'));
        foreach ($newData as $k => $v) {
            $v['taglist'] = isset($tagLists[$v['id']]) ? $tagLists[$v['id']] : [];
        }


        //推荐文章
        $recommData = $postModel->recommArticleLists($page, $pageSize, $routeData);
        //原创文章8篇
        $originData = $postModel->originArticleLists($page, 8, $routeData);
        //热门文章8篇
        $hotData = $postModel->hotArticleLists($page, 8, $routeData);
        //推荐标签
        $tagData = $tagModel->getTagLists($page, 10);
        $this->assign("newData", $newData);
        $this->assign("originData", $originData);
        $this->assign("hotData", $hotData);
        $this->assign("tagData", $tagData);
        $this->assign("recommData", $recommData);
        $this->assign('pages', $pages);

        return $this->fetch(':index');
    }


    /* author@zhou
     * 功能：导入同义词条
     * return 
     */
    public function importWord()
    {
        //存储字典
//        set_time_limit(0);
//        include_once EXTEND_PATH . "RedisDriver.php";
//        $redis = new \RedisDriver();
//
//        $myfile = fopen(EXTEND_PATH . "5.txt", "r");
//        while(! feof($myfile))
//        {
//            $row = explode("→", fgets($myfile));
//            $temp = [];
//            if (count($row) >= 2) {
//
//                $redis->hSet("yuanchuang",$row[0],$row[1]);
//                $redis->hSet("yuanchuang",$row[1],$row[0]);
////                foreach($row as $k=>$v){
////                    $key = array_rand($row,1);
////                    //输出随机内容
////                    $temp[$v]=$row[$key];
////                }
//            }
//
//          //  $redis->setHash("yuanchuang", $temp);
//            //setHash($key, $arr, $expire = null);
//        }


        //判断是否存储成功
        include_once EXTEND_PATH . "wordreplace/KeyWordReplace.php";
        include_once EXTEND_PATH . "RedisDriver.php";
        $redis = new \RedisDriver();
//        $row = $redis->hGet("yuanchuang","向往");
//        var_dump($row);
//
//        $row = $redis->hGet("yuanchuang","神往");
//        var_dump($row);


        //批量获取hash值
        $str = "虽然他看上去挺好的，但是别忘了男人很擅长逼你说分手，男人在这方面是天生的专家，他们能够冷暴力让你自己远离他，他们能够玩失踪点燃你内心的狂躁，等你忍无可忍，他就直接把责任推在你身上，你变得那么可恶，最后你可能是自己忍不住说分手。其实男人去拖延一个问题，是为了引诱你爆发，当你爆发的时候，他的罪恶感会降到最低，因为他能够给自己充分的理由离开你，因为你无理取闹，用这样的方式告诉自己，不是他狠心无情，而是你太作。";
        echo $str;
        echo "<hr/>";
        $words = $this->autoWord($str);
        $data = $redis->hMget("yuanchuang", $words);
        var_dump($data);
        echo "<hr/>";


        $key = new \KeyWordReplace($str, $data, false);
        echo $key->getResultText();

    }

    public function test2(){
        $article = "说到郁金香，很多人脑海可能是无尽的花海，是美丽的火焰纹，是漂亮的花束。其实关于郁金香还有个大名鼎鼎的事件，Tulip Mania，郁金香狂热，这个听起来像是一种癔症的名词现在已经成为了一个经济学术语。 本次文章前面讲一讲郁金香的历史，后面是郁金香的分类。
说到郁金香，很多人脑海可能是无尽的花海，是美丽的火焰纹，是漂亮的花束。其实关于郁金香还有个大名鼎鼎的事件，Tulip Mania，郁金香狂热，这个听起来像是一种癔症的名词现在已经成为了一个经济学术语。

本次文章分两个部分，前面讲一讲郁金香的历史，后面是郁金香的分类，对历史这部分没兴趣的可以略过，但是我觉得这部分挺有意思的。

进入欧洲

郁金香的栽培种最开始应该是在公元10世纪的波斯，最开始的品种一般是来自被种在花园里的自然杂交和芽变，大家觉得美丽的个体才会被挑出来。但是在早期的文献和资料里没有找到任何关于郁金香的记载。后来到了奥斯曼帝国时期，无数的郁金香被培养出来，直到今天，所有大类的郁金香仍然存在于土耳其。

公元1554年当时在奥斯曼帝国的比利时外交家布斯拜克（Busbecq）第一次把郁金香的种球和种子带入了欧洲，值得一提的是，随着他进入欧洲的并不是原生种，而是已经经过了高度人工驯化的种类，带回欧洲的种子实生苗包括了现在几乎所有的早花和中花类型，花的颜色和现代品种也相差不大，只是后期在花型和花茎上有所区别，也就是当时几乎已经有了现代郁金香的所有花色了，这也说明郁金香之前可能已经在土耳其栽培和人工选育了好几个世纪。

郁金香之父

布斯拜克将郁金香带到欧洲之后，他把一些种子也送给了他维亚纳的朋友-克卢修斯（Carolus Clusius，1526-1609）。



克卢修斯是提到郁金香不得不提到的一个人，他可以被称为16世纪影响力最大的园艺家，很多人都称他为“郁金香之父”，不仅发现了杂色郁金香，他甚至可以说是大名鼎鼎的郁金香狂热的“罪魁祸首”吧。

收到郁金香的时候克卢修斯在维也纳皇家花园做园丁，1573年他第一次种下了郁金香，之后他在1593年克卢修斯担任了荷兰莱顿大学植物园的主管，他随身带了一些郁金香的种球，克鲁修斯当时建了两个花园，一个是教学用的花园，另一个是他自己的私人花园，当然他把郁金香种到了私人园子里，这些种球在第二年就在荷兰开出了花朵，所以1574年也被认为是郁金香第一次在荷兰开花。就是这些在莱顿的郁金香种球，不仅为荷兰郁金香产业奠定了基础，而且引起了后来大名鼎鼎的“郁金香狂热”（Tulip mania）。

郁金香狂热

当时清新脱俗的郁金香很快就在莱顿的上层社会收到了热捧，使得不少人登门造访希望可以获得一些郁金香，不过克卢修斯不知道是因为啥原因，一直都不在愿意给，在利益的趋势下，开始有人不择手段了，在1596年和1598年，超过100个球被人从花园里偷走了。

想想，国内的兰花，日本的多肉十二卷被盗的事件，也算是历史重演了。

在令人震惊的“郁金香狂热”发生前，荷兰已经是世界贸易和金融中心，一个高度商业化的国家，当时荷属东印度公司和西印度公司垄断了东亚以及西半球的贸易，荷兰参与了无数的投机冒险活动并且取得了巨大的成功，每天都会有无数的新奇事物被带进荷兰港，有钱有闲的收藏家门搜集着各种各样的宝石、绘画、钱币甚至是狗，这是荷兰当之无愧的黄金时代。

狂热其实可以分为三个阶段：

第一阶段是供不应求导致高价

第二阶段是开始有投机者进入市场

第三阶段是平民开始卷入市场，之后即逐渐开始泡沫化，价格开始暴跌

最初，郁金香只是在植物学界收到人们的热捧，不久之后，欧洲的贵族们开始喜欢上这种花朵，他们认为这是一种高贵的花，可以突显他们的身份和地位。

不知何时，荷兰的上层社会形成了攀比的风气，很多人认为哪个有钱人家里如果没有收藏郁金香的种球，就证明这家人低劣。固化的印象让更多的人投入了这股疯狂的洪流之中。当时除了那些经典纯色品种之外，开始出现了杂色郁金香，这是一种因为郁金香碎色病毒（Tulip breaking virus）导致的病变，郁金香不再是单纯的红、黄或者白，而是出现了令人惊艳的条纹，它成为了红、白、紫、黄四色中任意两色的组合，这忽然出现的缤纷的花纹，就好像喷溅在各种颜料一样，这种羽毛或者火焰一样的纹路惊艳了整个荷兰，后来还出现了经典的紫-白条纹的“永恒的皇帝”（Semper Augustus）。



永恒的皇帝（Semper Augustus）

但是由于是病毒感染导致的，所以郁金香都会退化而且很难遗传。

整个荷兰都疯狂了，富人们竞相收购，不惜用农场、房子甚至女儿的婚事来交换，所有人都觉得这疯狂不会结束。

关于郁金香的传闻开始在平民间流传，吸引着他们也开始进入市场，平民因为缺乏资金，从最普通的品种买起，这也导致最普通的品种的价格也开始抬升，开始有不少因为倒货而获得利益的人，这之后，交易模式开始改变，全年交易和期货交易制度开始引进了。

查尔斯•麦凯在书中还记载下了这样一个故事，哈利姆的一个商人为了获得一个小小的郁金香球茎花掉了自己一半的家产，但他并不打算出手谋利，而是把它藏在自己的温室之中，以拥有它为荣。因此，这位商人变得远近闻名。

那时候的诗人们也无尽地赞美郁金香，形容它浑身上下活力四射、色彩鲜艳、独占花魁。这更加刺激了人们对郁金香的狂热。

各地的郁金香交易所建立起来了，交易只用提供证明“到时候交易“的票据和少量预付款就可以完成。这种方式吸引了更多的连资金都没有的人的进入，投机的症状开始在全荷兰显现了。股票投机商、中间人开始大量交易郁金香球根，这些人开始利用各种手段操纵市场，郁金香球根的价格开始跌宕起伏，精明的人开始利用价格的涨跌赚取大量的利润。在交易市场机制的助推下，郁金香球根的价值更加脱离了价格的现实，刺激着人们的欲望。

到1635的时候，有人宁愿花10万弗罗林(Florin，荷兰的官方流通货币)的价格购买40个郁金香球根。

狂热很快到了顶点，人们都沉浸在疯狂的幻想之中，没人想到灾难就忽然降临了。一直到今天，还是没有经济史学家说的清楚到底发生了什么，1637年2月，荷兰郁金香交易所一如既往的开张，但是忽然，买方出现了大量的抛售，市场忽然就进入了恐慌状态，郁金香的价格开始瞬间暴跌，人们的信心也一落千丈，贵族们眼睁睁的看着自己的财产灰飞烟灭。

一个个的城市陷入了混乱，朋友之间互相指责，邻居之间充满谩骂，不断的有人撕毁合同。政府也没有办法重新给人们找回信心，经此劫难，荷兰的商业系统受到了重大的打击，10多年以后才渐渐恢复。

这也就是人类历史上第一次泡沫经济，也是第一次让大众领略到集体疯狂和投机引起的悲剧。不过，人是会吸取教训的动物么？当然不会，100多年以后，荷兰再次因为洋水仙而重蹈覆辙。

直到今天，全世界仍然在迷恋着这种行为，可能是房子，可能是股票，也可能是其它的，关键是，所有身的人都觉得，这一切不会停止。

这一部分，其实我说的可能不只是郁金香。

🌷郁金香的分类

扯了这么多，还是言归正传一下，我觉得郁金香的种植，应该不需要我再多说了吧，正常拿到球，有霉斑的用杀菌剂浸泡一下，埋土里就好了，关于深度，参考下这张图好了。
        ";
        $s =$this->Translate($article);
        echo $s;
    }




}



