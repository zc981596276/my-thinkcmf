<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\portal\controller;

use cmf\controller\AdminBaseController;
use app\portal\model\PortalPostModel;
use app\portal\service\PostService;
use app\portal\model\PortalCategoryModel;
use think\Db;
use app\admin\model\ThemeModel;
use app\portal\model\PortalTdkModel;

class AdminTdkController extends AdminBaseController
{
    /**
     * 标签管理
     * @adminMenu(
     *     'name'   => 'TDK管理',
     *     'parent' => 'portal/AdminIndex/default',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => 'TDK规则列表',
     *     'param'  => ''
     * )
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $content = hook_one('portal_admin_tdk_index_view');

        if (!empty($content)) {
            return $content;
        }

        $param = $this->request->param();
        $name = $this->request->param("name");


        $postService = new PostService();
        $data        = $postService->adminTdkList($param);

        $data->appends($param);
        $this->assign('articles', $data->items());
        $this->assign('page', $data->render());
        $this->assign('name', $name);



        return $this->fetch();
    }


    //添加
    public function add()
    {
        $content = hook_one('portal_admin_tdk_add_view');

        if (!empty($content)) {
            return $content;
        }

        $themeModel        = new ThemeModel();
        $articleThemeFiles = $themeModel->getActionThemeFiles('portal/tdk/index');
        $this->assign('article_theme_files', $articleThemeFiles);
        return $this->fetch();
    }


    //ajax  添加
    public function addPost()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $portalPostModel = new PortalTdkModel();
            if (empty($data['name']) || empty($data['title_rule'])) {
                $this->error("名称和标题规则不能为空");
            }

            $row = [
                "name"=>$data['name'],
                "title_rule"=>$data['title_rule'],
                "keywords_rule"=>$data['keywords_rule'],
                "description_rule"=>$data['description_rule'],
                "filter_str"=>$data['filter_str'],
            ];
            $id = $portalPostModel->insertGetId($row);


            $this->success('添加成功!', url('AdminTdk/edit', ['id' => $id]));
        }

    }


    //编辑
    public function edit()
    {

        $content = hook_one('portal_admin_tdk_edit_view');

        if (!empty($content)) {
            return $content;
        }

        $id = $this->request->param('id', 0, 'intval');

        $portalPostModel = new PortalTdkModel();
        $post            = $portalPostModel->where('id', $id)->find();


        $themeModel        = new ThemeModel();
        $articleThemeFiles = $themeModel->getActionThemeFiles('portal/tdk/index');
        $this->assign('article_theme_files', $articleThemeFiles);
        $this->assign('row', $post);

        return $this->fetch();
    }

    public function editPost()
    {

        if ($this->request->isPost()) {
            $data = $this->request->param();
            $id = $this->request->param('id', 0, 'intval');
            $model  = new PortalTdkModel();
            $model->where(array("id"=>$id))->update($data);
            $this->success('保存成功!');

        }
    }



    public function delete()
    {
        $param           = $this->request->param();
        $portalPostModel = new PortalTdkModel();

        if (isset($param['id'])) {
            $id           = $this->request->param('id', 0, 'intval');
            $result       = $portalPostModel->where('id', $id)->find();
            $data         = [
                'object_id'   => $result['id'],
                'create_time' => time(),
                'table_name'  => 'portal_tdk',
                'name'        => $result['name'],
                'user_id'     => cmf_get_current_admin_id()
            ];
            $resultPortal = $portalPostModel
                ->where('id', $id)
                ->delete();
            if ($resultPortal) {

                Db::name('recycleBin')->insert($data);
            }
            $this->success("删除成功！", '');

        }

        if (isset($param['ids'])) {
            $ids     = $this->request->param('ids/a');
            $recycle = $portalPostModel->where('id', 'in', $ids)->select();
            $result  = $portalPostModel->where('id', 'in', $ids)->delete();
            if ($result) {
                foreach ($recycle as $value) {
                    $data = [
                        'object_id'   => $value['id'],
                        'create_time' => time(),
                        'table_name'  => 'portal_tdk',
                        'name'        => $value['name'],
                        'user_id'     => cmf_get_current_admin_id()
                    ];
                    Db::name('recycleBin')->insert($data);
                }
                $this->success("删除成功！", '');
            }
        }
    }
    
    
    




}
